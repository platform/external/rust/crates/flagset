# This file is generated by cargo2rulesmk.py --run --features .
# Do not modify this file as changes will be overridden on upgrade.

LOCAL_DIR := $(GET_LOCAL_DIR)
MODULE := $(LOCAL_DIR)
MODULE_CRATE_NAME := flagset
MODULE_SRCS := \
	$(LOCAL_DIR)/src/lib.rs \

MODULE_RUST_EDITION := 2018
MODULE_LIBRARY_DEPS := \
	external/rust/crates/serde \

include make/library.mk
